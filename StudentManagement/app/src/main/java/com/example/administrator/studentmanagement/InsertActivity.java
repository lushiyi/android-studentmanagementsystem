package com.example.administrator.studentmanagement;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.administrator.studentmanagement.sqlite.DBManager;
import com.example.administrator.studentmanagement.sqlite.stClass;

import java.util.ArrayList;

public class InsertActivity extends AppCompatActivity {
    private DBManager dbManager;                        // 数据库管理类
    
    private Button btnInsert;
    private EditText ETnumber;
    private EditText ETname;

    private Spinner SPmajor;
    private Spinner SPyear;

    public static final String[] MAJOR_TYPE = {"物理专业", "化学专业", "计算机专业"};
    public static final String[] YARY_TYPE = {"2000", "2001", "2002"};

    private RadioButton RBbtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);

        dbManager = new DBManager(this);

        ETname = (EditText) findViewById(R.id.editText_name);
        ETnumber = (EditText) findViewById(R.id.editText_number);
        SPmajor = (Spinner)findViewById(R.id.spinner_major);
        SPyear = (Spinner)findViewById(R.id.spinner_year);

        ArrayAdapter<String> adapter;
        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, MAJOR_TYPE);
        SPmajor.setAdapter(adapter);
        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, YARY_TYPE);
        SPyear.setAdapter(adapter);

        RBbtn = (RadioButton) findViewById(R.id.radioButton_1) ;

        btnInsert = (Button)findViewById(R.id.button_insert);
        btnInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<stClass> stclasses = new ArrayList<stClass>();
//                (String st_number, String st_name, String st_major, int st_year, int st_sex ,String st_speciality)
                String st_speciality = "";
                stClass stclass = new stClass(ETnumber.getText().toString().trim(),
                        ETname.getText().toString().trim(),
                        SPmajor.getSelectedItem().toString(),
                        Integer.parseInt(SPyear.getSelectedItem().toString()),
                        RBbtn.isChecked()? 1:0,
                        st_speciality);
                stclasses.add(stclass);
                dbManager.add(stclasses);
                ToastDialog("添加成功");
            }
        });
        
    }



    // 提示框
    private void ToastDialog(String strMessage) {
        //调用显示Toast对话框
        Toast.makeText(this, strMessage, Toast.LENGTH_LONG).show();
    }
}
