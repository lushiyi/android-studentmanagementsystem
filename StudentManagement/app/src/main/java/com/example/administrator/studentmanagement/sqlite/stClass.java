package com.example.administrator.studentmanagement.sqlite;

/**
 * Created by Administrator on 2017/3/28.
 */

public class stClass {
    public int      st_id;                   //  索引
    public String   st_number;               //  学号
    public String   st_name;              //  姓名
    public String   st_major;             //  专业
    public int      st_year;                 //  年级
    public int      st_sex;                  //  性别
    public String   st_speciality;        //  特长

    public stClass()
    {

    }

    public stClass(String st_number, String st_name, String st_major, int st_year, int st_sex ,String st_speciality)
    {
        this.st_number          = st_number;
        this.st_name            = st_name;
        this.st_major           = st_major;
        this.st_year            = st_year;
        this.st_sex             = st_sex;
        this.st_speciality      = st_speciality;

    }
}
