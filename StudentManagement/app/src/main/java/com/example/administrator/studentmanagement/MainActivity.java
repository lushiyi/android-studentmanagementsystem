package com.example.administrator.studentmanagement;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Button BtnManage;
    private Button BtnSelect;
    private Button BtnInsert;

    private TextView TVUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bindingControl();
        bindingEvent();
    }

    // 绑定控件
    private void bindingControl(){
        BtnManage = (Button) findViewById(R.id.button_manage);
        BtnSelect = (Button) findViewById(R.id.button_select);
        BtnInsert = (Button) findViewById(R.id.button_insert);
        TVUser = (TextView) findViewById(R.id.textView_user);

        TVUser.setText("张三");
    }

    // 绑定事件
    private void bindingEvent(){
        BtnManage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //声明Intent对象，并启动 Activity
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, ManageActivity.class);
                startActivity(intent);
            }
        });
        BtnSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //声明Intent对象，并启动 Activity
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, SelectActivity.class);
                startActivity(intent);
            }
        });
        BtnInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //声明Intent对象，并启动 Activity
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, InsertActivity.class);
                startActivity(intent);
            }
        });
    }

}
