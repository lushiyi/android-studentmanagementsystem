package com.example.administrator.studentmanagement;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    private Button BtuLogin;
    private Button BtuExit;
    private Button BtuReg;
    private Button BtuBak;

    private CheckBox CBSave;

    private TextView TVAccount;
    private TextView TVPassword;

    SharedPreferences mySharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //实例化SharedPreferences对象（第一步）
        mySharedPreferences= getSharedPreferences("userInfo", Activity.MODE_PRIVATE);
        //实例化SharedPreferences.Editor对象（第二步）
        editor = mySharedPreferences.edit();


        bindingControl();
        bindingEvent();

        getPasswor();
    }

    // 绑定控件
    private void bindingControl(){
        BtuBak = (Button) findViewById(R.id.button_bak);
        BtuExit = (Button) findViewById(R.id.button_exit);
        BtuReg = (Button) findViewById(R.id.button_Reg);
        BtuLogin = (Button) findViewById(R.id.button_login);

        CBSave = (CheckBox) findViewById(R.id.checkBox_save);

        TVAccount = (TextView) findViewById(R.id.editText_account);
        TVPassword = (TextView) findViewById(R.id.editText_password);
    }

    // 绑定事件
    private void bindingEvent(){
        BtuBak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        BtuExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        BtuReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //声明Intent对象，并启动 Activity
                Intent intent = new Intent();
                intent.setClass(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });
        BtuLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TVAccount.getText().length() >= 6 && TVPassword.getText().length() >= 6){
                    loginSucceed();
                    savePassword();
                }
            }
        });

    }


    //登陆成功
    private void loginSucceed(){
        //声明Intent对象，并启动 Activity
        Intent intent = new Intent();
        intent.setClass(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    //保存密码
    private void savePassword(){
        //用putString的方法保存数据
        editor.putString("account", TVAccount.getText().toString());
        editor.putString("password", TVPassword.getText().toString());
        //提交当前数据
        editor.commit();
    }

    //提取密码
    private void getPasswor(){
        TVAccount.setText(mySharedPreferences.getString("account",""));
        TVPassword.setText(mySharedPreferences.getString("password",""));
    }

    // 提示框
    private void ToastDialog(String strMessage) {
        //调用显示Toast对话框
        Toast.makeText(this, strMessage, Toast.LENGTH_LONG).show();
    }

}
