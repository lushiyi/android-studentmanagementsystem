package com.example.administrator.studentmanagement;

import android.app.Activity;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RegisterActivity extends AppCompatActivity {

//    SharedPreferences mySharedPreferences;
//    SharedPreferences.Editor editor;

    private Button btn_register;
    private EditText TVRegAccount;
    private EditText TVRegUserName;
    private EditText TVRegPassword;
    private EditText TVRegPassword2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

//        //实例化SharedPreferences对象（第一步）
//        mySharedPreferences= getSharedPreferences("userInfo", Activity.MODE_PRIVATE);
//        //实例化SharedPreferences.Editor对象（第二步）
//        editor = mySharedPreferences.edit();

        TVRegAccount = (EditText) findViewById(R.id.editText_RegAccount);
        TVRegUserName = (EditText) findViewById(R.id.editText_RegUserName);
        TVRegPassword = (EditText) findViewById(R.id.editText_RegPassword);
        TVRegPassword2 = (EditText) findViewById(R.id.editText_RegPassword2);


        btn_register = (Button) findViewById(R.id.button_register);
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TVRegAccount.getText().length() == 0 ||
                        TVRegUserName.getText().length() == 0 ||
                        TVRegPassword.getText().length() < 6 ||
                        TVRegPassword2.getText().length() < 6){
                    ToastDialog("输入不规范 或不完整");
                }



            }
        });
    }


    private void ToastDialog(String strMessage) {
        //调用显示Toast对话框
        Toast.makeText(this, strMessage, Toast.LENGTH_LONG).show();
    }
}
