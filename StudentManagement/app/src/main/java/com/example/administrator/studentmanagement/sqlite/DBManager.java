package com.example.administrator.studentmanagement.sqlite;

/**
 * Created by Administrator on 2017/3/28.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class DBManager {
    private  Database helper;
    private  SQLiteDatabase db;
    private  String LOG_TAG = "DBManager";

    public DBManager(Context context)
    {
        Log.d(LOG_TAG, "DBManager --> Constructor");
        helper = new Database(context);
        // 因为getWritableDatabase内部调用了mContext.openOrCreateDatabase(mName, 0,
        // mFactory);
        // 所以要确保context已初始化,我们可以把实例化DBManager的步骤放在Activity的onCreate里
        db = helper.getWritableDatabase();
    }

    /**
     * close database
     */
    public void closeDB()
    {
        Log.d(LOG_TAG, "DBManager --> closeDB");
        // 释放数据库资源
        db.close();
    }

    /**
     * add stClass
     *
     * @param stclasses
     */
    public void add(List<stClass> stclasses)
    {
        Log.d(LOG_TAG, "DBManager --> add");
        // 采用事务处理，确保数据完整性
        db.beginTransaction(); // 开始事务
        try
        {
            for (stClass stclass : stclasses)
            {
                db.execSQL("INSERT INTO " + Database.TABLE_NAME
                        + " VALUES(null, ?, ?, ?, ?, ?, ?)", new Object[] { stclass.st_number,
                        stclass.st_name, stclass.st_major, stclass.st_year,
                        stclass.st_sex,stclass.st_speciality});
                // 带两个参数的execSQL()方法，采用占位符参数？，把参数值放在后面，顺序对应
                // 一个参数的execSQL()方法中，用户输入特殊字符时需要转义
                // 使用占位符有效区分了这种情况
            }
            db.setTransactionSuccessful(); // 设置事务成功完成
        }
        finally
        {
            db.endTransaction(); // 结束事务
        }
    }

    /**
     * update
     *
     * @param stclass
     */
    public void update(stClass stclass)
    {
        Log.d(LOG_TAG, "DBManager --> updateAge");
        ContentValues cv = new ContentValues();
        cv.put("st_number", stclass.st_number);
        cv.put("st_name", stclass.st_name);
        cv.put("st_major", stclass.st_major);
        cv.put("st_year", stclass.st_year);
        cv.put("st_sex", stclass.st_sex);
        cv.put("st_speciality", stclass.st_speciality);
        db.update(Database.TABLE_NAME, cv, "st_id = ?",
                new String[] { String.valueOf(stclass.st_id)});
    }

//    /**
//     * update count
//     *
//     * @param stclass
//     */
//    public void updateCount(stClass stclass)
//    {
//        Log.d(LOG_TAG, "DBManager --> update");
//        ContentValues cv = new ContentValues();
//        cv.put("st_count", stclass.st_count ++);
//        db.update(Database.TABLE_NAME, cv, "st_id = ?",
//                new String[] { String.valueOf(stclass.st_id)});
//    }

    /**
     * delete old stclass
     *
     * @param st_ids
     */
    public void delete(List<Integer> st_ids)
    {
        Log.d(LOG_TAG, "DBManager --> deleteOldPerson");
        for (Integer st_id: st_ids) {
            db.delete(Database.TABLE_NAME, "st_id == ?",
                    new String[] { String.valueOf(st_id) });
        }
    }

    /**
     * query all persons, return list
     *
     * @return List<Person>
     */
    public List<stClass> query()
    {
        Log.d(LOG_TAG, "DBManager --> query");
        ArrayList<stClass> stclasses = new ArrayList<stClass>();
        Cursor c = queryTheCursor();
        while (c.moveToNext())
        {
            stClass stclass = new stClass();
            stclass.st_id       = c.getInt(c.getColumnIndex("st_id"));
            stclass.st_number  = c.getString(c.getColumnIndex("st_number"));
            stclass.st_name = c.getString(c.getColumnIndex("st_name"));
            stclass.st_major = c.getString(c.getColumnIndex("st_major"));
            stclass.st_year   = c.getInt(c.getColumnIndex("st_year"));
            stclass.st_sex  = c.getInt(c.getColumnIndex("st_sex"));
            stclass.st_speciality    = c.getString(c.getColumnIndex("st_speciality"));
            stclasses.add(stclass);
        }
        c.close();
        return stclasses;
    }

    /**
     * query all stclass, return cursor
     *
     * @return Cursor
     */
    public Cursor queryTheCursor()
    {
        Log.d(LOG_TAG, "DBManager --> queryTheCursor");
        Cursor c = db.rawQuery("SELECT * FROM " + Database.TABLE_NAME + " ORDER BY st_count DESC",
                null);
        return c;
    }
}
